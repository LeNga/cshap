﻿using ModelEF.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelEF.Model;
using TestUngDung.Common;
using PagedList;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        // GET: Admin/User
        public ActionResult Index(int page = 1, int pagesize = 5)
        {
            var user = new UserDao();
            var model = user.ListAll();
            return View(model.ToPagedList(page, pagesize));
        }
        [HttpPost]
        public ActionResult Index(string searchString, int page = 1, int pagesize = 5)
        {
            var user = new UserDao();
            var model = user.ListWhereAll(searchString);
            ViewBag.SearchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(UserAccount model)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                if (dao.Check(model.UserName) != null)
                {
                    SetAlert("Tên đăng nhập đã tồn tại. Mời nhập tên đăng nhập khác!", "warning");
                    return RedirectToAction("Create", "User");
                }
                else
                {
                    var pass = Encryptor.EncryptorMD5(model.Password);
                    model.Password = pass;
                    string result = dao.Insert(model);
                    if (!string.IsNullOrEmpty(result))
                    {
                        SetAlert("Thêm thành công!", "success");
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        SetAlert("Thêm mới không thành công!", "error");
                    }
                }                
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(string key)
        {
            var dao = new UserDao();
            var result = dao.Check(key);
            if (result != null) return View(result);
            return View();
        }
        [HttpPost]
        public ActionResult Edit(UserAccount model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dao = new UserDao();
                    var pass = Encryptor.EncryptorMD5(model.Password);
                    model.Password = pass;
                    string result = dao.Update(model);
                    if (!string.IsNullOrEmpty(result))
                    {
                        SetAlert("Cập nhật thành công!", "success");
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        SetAlert("Cập nhât không thành công!", "error");
                    }
                }
            }
            catch (Exception ex)
            {
                Common.Common.WriteLog("Sach", "Edit-Post", ex.ToString());
            }
            return View();
        }
        [HttpDelete]
        public ActionResult Delete(UserAccount model)
        {            
                var dao = new UserDao().Delete(model);
                SetAlert("Xóa thành công!", "error");
                return RedirectToAction("Index","User");

        }
    }
}