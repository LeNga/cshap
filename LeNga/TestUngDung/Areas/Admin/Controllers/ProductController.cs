﻿using ModelEF.DAO;
using ModelEF.Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Admin/Product
        public ActionResult Index(int page = 1, int pagesize = 5)
        {
            var sach = new ProductDao();
            var model = sach.ListAll();
            return View(model.ToPagedList(page, pagesize));
        }
        [HttpPost]
        public ActionResult Index(string searchString, int page = 1, int pagesize = 5)
        {
            var sach = new ProductDao();
            var model = sach.ListWhereAll(searchString);
            ViewBag.SearchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }
        [HttpGet]
        public ActionResult Create()
        {
            SetViewBag();
            Product pro = new Product();
            return View(pro);
        }        
        [HttpPost]
        public ActionResult Create(Product model, HttpPostedFileBase img)
        {
            SetViewBag();
            if (ModelState.IsValid)
            {
                var dao = new ProductDao();
                if (img != null && img.ContentLength > 0)
                {                    
                    string result = dao.Insert(model);
                    var fileName = Path.GetFileName(img.FileName);
                    string path = Path.Combine(Server.MapPath("//Assest/Client/images/"), fileName);
                    img.SaveAs(path);
                    fileName = "/Assest/Client/images/" + fileName;
                    dao.UploadImg(fileName, model.ProductId);
                    if (!string.IsNullOrEmpty(result))
                    {
                        SetAlert("Thêm thành công!", "success");
                        return RedirectToAction("Index", "Product");
                    }
                    else
                    {
                        SetAlert("Thêm mới không thành công!", "error");
                    }
                }
                else
                {
                    SetAlert("Upload hình không thành công!", "error");
                    return RedirectToAction("Create", "Product");
                }                
            }
            return View();
        }
        [HttpGet]
        public ActionResult Edit(int key)
        {
            var dao = new ProductDao();
            var result = dao.Find(key);
            SetViewBag(result.CategoryId);
            if (result != null) return View(result);
            return View();
        }
        public ActionResult Detail(int key)
        {
            var sach = new ProductDao();
            var model = sach.Detail(key);
            return View(model.FirstOrDefault());
        }
        public ActionResult Edit(Product model, HttpPostedFileBase editimg)
        {
            try
            {
                SetViewBag();
                if (ModelState.IsValid)
                {
                    var dao = new ProductDao();
                    if (editimg != null && editimg.ContentLength > 0)
                    {
                        int result = dao.Update(model);
                        var fileName = Path.GetFileName(editimg.FileName);
                        string path = Path.Combine(Server.MapPath("//Assest/Client/images/"), fileName);
                        editimg.SaveAs(path);
                        fileName = "/Assest/Client/images/" + fileName;
                        dao.UploadImg(fileName, model.ProductId);
                        if (result != 0)
                        {
                            SetAlert("Chỉnh sửa thành công!", "success");
                            return RedirectToAction("Index", "Product");
                        }
                        else
                        {
                            SetAlert("Chỉnh sửa không thành công!", "error");
                        }
                    }
                    else
                    {
                        SetAlert("Upload hình không thành công!", "error");
                        return RedirectToAction("Edit", "Product");
                    }
                }                
            }
            catch (Exception ex)
            {
                Common.Common.WriteLog("Sach", "Edit-Post", ex.ToString());
            }
            return View();
        }
        [HttpDelete]
        public ActionResult Delete(Product model)
        {
            var dao = new ProductDao().Delete(model);
            return RedirectToAction("Index");
        }
        public void SetViewBag(int? selectedId = null )
        {
            var dao = new CategoryDao();
            ViewBag.CategoryID = new SelectList(dao.ListAll(), "CategoryID", "CategoryName", selectedId);
        }
        
    }
}