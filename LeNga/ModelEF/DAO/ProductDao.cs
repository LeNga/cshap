﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class ProductDao
    {
        private LeNgaContext db;
        public ProductDao()
        {
            db = new LeNgaContext();
        }
        public List<Product> ListAll()
        {
            return db.Products.OrderBy(s => s.Quantity).ThenByDescending(s => s.UnitCost).ToList();
        }
        public IEnumerable<Product> ListWhereAll(string keysearch)
        {
            IQueryable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(keysearch))
            {
                model = model.Where(s => s.Name.Contains(keysearch) || s.UnitCost.ToString().Contains(keysearch) ||
                s.Category.CategoryId.ToString().Contains(keysearch) || s.Quantity.ToString().Contains(keysearch));
            }
            return model.OrderBy(s => s.Quantity).ThenByDescending(s => s.UnitCost);
        }
        public List<Product> ListSale(int top)
        {
            return db.Products.Where(s => s.Status == true).Take(top).ToList();
        }
        public List<Product> ListHot(int top)
        {
            return db.Products.Where(s => s.Quantity >= 50).Take(top).ToList();
        }
        public List<Product> Detail(int id)
        {
            return db.Products.Where(s => s.ProductId == id).ToList();
        }
        public Product Find(int productid)
        {
            return db.Products.Find(productid);
        }        
        public string Insert(Product entityProduct)
        {
            db.Products.Add(entityProduct);
            db.SaveChanges();
            return entityProduct.ProductId.ToString();
        }
        public int Update(Product entityProduct)
        {
            var product = Find(entityProduct.ProductId);
            if (product == null)
            {
                db.Products.Add(entityProduct);
            }
            else
            {
                product.Name = entityProduct.Name;
                product.UnitCost = entityProduct.UnitCost;
                product.Quantity = entityProduct.Quantity;
                product.Status = entityProduct.Status;
                product.CategoryId = entityProduct.CategoryId;
            }
            db.SaveChanges();
            return entityProduct.ProductId;
        }
        public int Delete(Product entityProduct)
        {
            try
            {
                var product = Find(entityProduct.ProductId);
                db.Products.Remove(product);
                db.SaveChanges();
                return entityProduct.ProductId;
            }
            catch (Exception ex)
            {
                return entityProduct.ProductId;
            }
        }
        public void UploadImg (string filename, int id)
        {
            var product = db.Products.FirstOrDefault(s => s.ProductId == id);
            product.Image = filename;
            db.SaveChanges();
        }
    }
}
