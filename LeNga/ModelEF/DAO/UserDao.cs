﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class UserDao
    {
        private LeNgaContext db;
        public UserDao()
        {
            db = new LeNgaContext();
        }
        public int login(string user, string pass)
        {
            var result = db.UserAccounts.SingleOrDefault(x => x.UserName.Contains(user) 
            && x.Password.Contains(pass) && x.Status.Contains("activity") && x.Role.Contains("admin"));
            if (result == null)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public List<UserAccount> ListAll()
        {
            IQueryable<UserAccount> model = db.UserAccounts;
            model = model.Where(s => s.Status.Contains("activity"));
            return model.OrderBy(s => s.UserId).ToList();
        }
        public IEnumerable<UserAccount> ListWhereAll(string keysearch)
        {
            IQueryable<UserAccount> model = db.UserAccounts;
            if (!string.IsNullOrEmpty(keysearch))
            {
                model = model.Where(s => s.UserName.Contains(keysearch) || s.Status.Contains(keysearch)
                || s.Role.Contains(keysearch));
            }
            return model.OrderBy(s => s.UserId);
        }
        public UserAccount Check(string username)
        {
            IQueryable<UserAccount> model = db.UserAccounts;
            foreach (var item in model)
            {
                if (item.UserName.Equals(username))
                {
                    return item;
                }
            }
            return null;
        }
        public string Insert(UserAccount entityUser)
        {
            db.UserAccounts.Add(entityUser);
            db.SaveChanges();
            return entityUser.UserName;
        }

        public string Update(UserAccount entityUser)
        {
            var user = Check(entityUser.UserName);
            if (user == null)
            {
                db.UserAccounts.Add(entityUser);
            }
            else
            {
                user.UserName = entityUser.UserName;
                user.Password = entityUser.Password;
                user.Status = entityUser.Status;
                user.Role = entityUser.Role;
            }
            db.SaveChanges();
            return entityUser.UserName;
        }

        public string Delete(UserAccount entityUser)
        {
            try
            {
                var user = Check(entityUser.UserName);
                var status = entityUser.Status;
                db.UserAccounts.Remove(user);
                db.SaveChanges();
                return entityUser.UserName;
            }
            catch (Exception ex)
            {
                return entityUser.UserName;
            }
        }
    }
}
