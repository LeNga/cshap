﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelEF.DAO
{
    public class CategoryDao
    {
        private LeNgaContext db = null;
        public CategoryDao()
        {
            db = new LeNgaContext();
        }
        public List<Category> ListAll()
        {
            return db.Categories.ToList();
        }
        public Product Find(int id)
        {
            return db.Products.Find(id);
        }
    }
}
