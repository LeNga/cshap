﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAccount")]
    public partial class UserAccount
    {
        [Key]
        [DisplayName("ID")]
        public int UserId { get; set; }
        [Required(ErrorMessage ="Khổng để trống tên đăng nhập")]
        [StringLength(50)]

        [DisplayName("Tên đăng nhập")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Khổng để trống mật khẩu")]
        [StringLength(50)]
        [DisplayName("Mật khẩu")]
        public string Password { get; set; }
        [Required (ErrorMessage ="Không để trống trạng thái")]
        [StringLength(20)]
        [DisplayName("Trạng thái")]
        public string Status { get; set; }
        [Required(ErrorMessage ="Không để trống quyền hạn")]
        [StringLength(20)]
        [DisplayName("Quyền hạn")]
        public string Role { get; set; }
    }
}
