﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    [Table("Product")]
    public partial class Product
    {
       [Key]
       [DisplayName("ID")]
        public int ProductId { get; set; }

        [Required(ErrorMessage ="Không để trống Tên sản phẩm")]
        [StringLength(50)]
        [DisplayName("Tên sản phẩm")]
        public string Name { get; set; }
        [DisplayName("Giá")]  
        public decimal UnitCost { get; set; }
        [DisplayName("Số lượng")]
        public int Quantity { get; set; }
        [DisplayName("Hình ảnh")]
        [StringLength(255)]
        public string Image { get; set; }
        [DataType(DataType.Upload)]
        [DisplayName("Mô tả")]
        [StringLength(50)]
        [Required(ErrorMessage = "Vui lòng chọn ảnh")]
        public string Description { get; set; }
        [DisplayName("Trạng thái")]
        [Required(ErrorMessage = "Không để trống trạng thái")]
        public bool? Status { get; set; }
        [DisplayName("Thể loại")]
        [Required(ErrorMessage ="Không để trống Thể loại")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

    }
}
