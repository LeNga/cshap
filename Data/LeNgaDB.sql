﻿Create database LeNgaDB
Use LeNgaDB
CREATE TABLE UserAccount(
	UserId int IDENTITY(1,1) primary key,
	UserName nvarchar(50) not null,
	Password nvarchar(50) not null,
	Status nvarchar(20) not null, 
	Role nvarchar(20) not null,
	)
CREATE TABLE Category(
	CategoryId int IDENTITY(1,1) primary key,
	CategoryName nvarchar(20) not null,
	Description  nvarchar(50) 
)
CREATE TABLE Product(
	ProductId int IDENTITY(1,1) primary key,
	Name nvarchar(50) not null,
	UnitCost decimal(10,2) not null,
	Quantity int not null,
	Image  nvarchar(255),
	Description  nvarchar(50) null ,
	Status bit default 1,
	CategoryId int not null,
	CONSTRAINT fk_Category FOREIGN KEY (CategoryId) REFERENCES Category (CategoryId)
)


